export LC_ALL=C

# nicer colors
if [ "$TERM" = "linux" ]; then
    echo -en "\e]P232323" #black
    echo -en "\e]P72028C" #darkgrey
    echo -en "\e]P4E9A06" #darkred
    echo -en "\e]PFn57900" #red
    echo -en "\e]P000E99" #darkgreen
    echo -en "\e]P9300B0" #green
    echo -en "\e]P00B972" #brown
    echo -en "\e]PF5F5F5" #yellow
    echo -en "\e]P708284" #darkblue
    echo -en "\e]PFF00C7" #blue
    echo -en "\e]P8AE234" #darkmagenta
    echo -en "\e]PFFC135" #magenta
    echo -en "\e]P42A5F5" #darkcyan
    echo -en "\e]PFF00BD" #cyan
    echo -en "\e]P2DED79" #lightgrey
    echo -en "\e]PF5F5F5" #white
    clear #for background artifacting
fi

# Launch tmux at startup
tmux -f "$HOME/.tmux.conf"
clear

# aliases
alias lsblk="lsblk -o NAME,SIZE,FSTYPE,TYPE,RO"

# Show devices
lsblk
echo

echo -n "IP "
/usr/bin/ip-screen
echo
