#!/usr/bin/env bash

ISO_MASTER="grml96-full_2018.12.iso" # grml64-full_2018.12.iso

# TODO command line parsing
# TODO ISO testing
# sudo apt-get install xorriso
# xorriso

# List of GRML cheat codes to inject
# see http://git.grml.org/?p=grml-live.git;a=blob_plain;f=templates/GRML/grml-cheatcodes.txt;hb=HEAD
GRMLCODES="ssh=grml lang=de nofb keyboard=de nobeep lvm noraid noblank noquick passwd= config=config.tbz scripts=grml-custom-boot"
# startup=/usr/bin/ip-screen nofb vga=ask
DEBS=(clonezilla ripgrep byobu task-spooler)

OPT_APT_UPDATE=1
OPT_APT_UPGRADE=0

# Strict bash scripting
set -euo pipefail -o errtrace

# Move the cursor up N lines
C_UP="\e[1A"
# Move the cursor down N lines
C_DOWN="\e[1B"
# Move the cursor back N columns
C_BACK="\e[1D"
# Move the cursor forward N columns
C_FORWARD="\e[1C"
# Erase to end of line
C_ERASE="\e[K"
# Carriage return
C_RETURN="\r"
# Save cursor position
C_SAVE="\e[s"
# Restore cursor position
C_RESTORE="\e[u"

# Colors
C_RESET="\033[0m"
C_BRIGHT="\e[1m"
C_RED="\e[31m"
C_YELLOW="\e[33m"
C_BYELLOW="${C_BRIGHT}\e[93m"
C_BWHITE="${C_BRIGHT}\e[97m"
C_MAGENTA="\e[35m"
C_UNDERLINE="\e[4m"
C_NO_UNDERLINE="\e[24m"
SHUTDOWN="EXIT"

# if we need to upgrade update apt cache
if [[ "${OPT_APT_UPGRADE}" -eq 1 ]]; then
  OPT_APT_UPDATE=1
fi

# If we have debs do update
if [[ "${#DEBS}" -gt 0 ]]; then
  OPT_APT_UPDATE=1
fi

##
# Functions

cleanup() {
  if [[ "${_CLEANUP:-0}" -eq 1 ]]; then
    return
  fi
  export _CLEANUP=1

  echotitle "Cleaning up"
  unmount_everything 
  sleep 1
  if [[ -d "${TMP}" ]]; then
    sudo rm -fr "${TMP}" || true
  fi
  echo "Temporary files removed"
}


press_any_key() {
  echo -en "${C_YELLOW}${C_BRIGHT}Press RETURN key to continue!${C_RESET}"
  echo
  tput civis
  read
  tput cnorm	
}

execinfo() {
  echoinfo "[EXEC] ${*}" | sed 's/sudo //g' >&3
  eval $@
}

pipe_handler_error() {
	# while read line; do
	while IFS= read -r line; do
	# while IFS= read -rn1 line; do
		echo -e "${C_MAGENTA}${C_BRIGHT}${line}${C_RESET}"
	done
}

exitcode_to_string() {
	code=$1
	case 1 in
	$(($code == 0))) echo "successful termination (EX_OK)" ;;
	$(($code == 1))) echo "general error" ;;
	$(($code == 2))) echo "misuse of shell builtins" ;;
	$(($code == 64))) echo "command line usage error (EX_USAGE)" ;;
	$(($code == 65))) echo "data format error (EX_DATAERR)" ;;
	$(($code == 66))) echo "cannot open input (EX_NOINPUT)" ;;
	$(($code == 67))) echo "addressee unknown (EX_NOUSER)" ;;
	$(($code == 68))) echo "host name unknown (EX_NOHOST)" ;;
	$(($code == 69))) echo "service unavailable (EX_UNAVAILABLE)" ;;
	$(($code == 70))) echo "internal software error (EX_SOFTWARE)" ;;
	$(($code == 71))) echo "system error (e.g., can't fork) (EX_OSERR)" ;;
	$(($code == 72))) echo "critical OS file missing (EX_OSFILE)" ;;
	$(($code == 73))) echo "can't create (user) output file (EX_CANTCREAT)" ;;
	$(($code == 74))) echo "input/output error (EX_IOERR)" ;;
	$(($code == 75))) echo "temp failure; user is invited to retry (EX_TEMPFAIL)" ;;
	$(($code == 76))) echo "remote error in protocol (EX_PROTOCOL)" ;;
	$(($code == 77))) echo "permission denied (EX_NOPERM)" ;;
	$(($code == 78))) echo "configuration error" ;;
	$(($code == 88))) echo "halt" ;;
	$(($code == 126))) echo "command invoked cannot execute" ;;
	$(($code == 127))) echo "command not found" ;;
	$(($code == 128))) echo "invalid exit argument" ;;
	$(($code == 129))) echo "Hangup" ;;
	$(($code == 130))) echo "Interrupt" ;;
	$(($code == 131))) echo "quit and dump core" ;;
	$(($code == 132))) echo "illegal instruction" ;;
	$(($code == 133))) echo "Trace/breakpoint trap" ;;
	$(($code == 134))) echo "process aborted" ;;
	$(($code == 135))) echo "bus error: \"access to undefined portion of memory object\"" ;;
	$(($code == 136))) echo "floating point exception: \"erroneous arithmetic operation\"" ;;
	$(($code == 137))) echo "kill (terminate immediately)" ;;
	$(($code == 138))) echo "user-defined 1" ;;
	$(($code == 139))) echo "segmentation violation" ;;
	$(($code == 140))) echo "user-defined 2" ;;
	$(($code == 141))) echo "write to pipe with no one reading" ;;
	$(($code == 142))) echo "signal raised by alarm" ;;
	$(($code == 143))) echo "termination (request to terminate)" ;;
	# 144 - not defined
	$(($code == 145))) echo "child process terminated, stopped (or continued*)" ;;
	$(($code == 146))) echo "continue if stopped" ;;
	$(($code == 147))) echo "stop executing temporarily" ;;
	$(($code == 148))) echo "terminal stop signal" ;;
	$(($code == 149))) echo "background process attempting to read from tty (\"in\")" ;;
	$(($code == 150))) echo "background process attempting to write to tty (\"out\")" ;;
	$(($code == 151))) echo "urgent data available on socket" ;;
	$(($code == 152))) echo "CPU time limit exceeded" ;;
	$(($code == 153))) echo "file size limit exceeded" ;;
	$(($code == 154))) echo "signal raised by timer counting virtual time: \"virtual timer expired\"" ;;
	$(($code == 155))) echo "profiling timer expired" ;;
	# 156 - not defined
	$(($code == 157))) echo "pollable event" ;;
	# 158 - not defined
	$(($code == 159))) echo "bad syscall" ;;
	$(($code > 128 && $code < 255))) echo "fatal error signal" ;;
	$(($code == 255))) echo "exit status out of range" ;;
	*) echo "unknown" ;;
	esac
}

get_error_string() {
  local errorString="unknown"
  local errorCode="${1}"
  if [ "${DEBUG:-0}" -eq 1 ] && [ "${errorCode}" == "?" ]; then
          errorString="debug"
  elif [ "${errorCode}" != "?" ]; then
          errorString="error code $(exitcode_to_string ${errorCode})"
  fi
  echo $errorString
}

backtrace() {
  set +o xtrace
  local errorCode="${1:-?}"
  local errorString=$(get_error_string "$errorCode")
  echoerr "${C_RED}${C_BRIGHT}Command '${_BASH_COMMAND:-${BASH_COMMAND}}' exited with \"${errorString}\" (${errorCode})${C_RESET}"
  
  # Print out the stack trace described by $function_stack
  if [ "${#FUNCNAME[@]}" -gt 1 ]; then
          echoerr "\n${C_RED}Call tree:"
          for ((i = 1; i < "${#FUNCNAME[@]}" -1 ; i++)); do
                  echoerr "  $i: ${BASH_SOURCE[$i + 1]}:${BASH_LINENO[$i]} ${FUNCNAME[$i+1]}(...)"
          done
          echoerr "${C_RESET}"
  fi
  export SHUTDOWN="ERR"
}

shutdown() {
  local errorCode="$?"
  cleanup
  if [[ "${SHUTDOWN}" == "ERR" ]] && [[ "${SHUTDOWN}" == "EXIT" ]]; then
     echo "${C_RED}${C_BRIGHT}Exiting with \"$(get_error_string "${errorCode}")\" (${errorCode})${C_RESET}"
  fi
  if [[ "${SHUTDOWN}" == "INT" ]]; then
    echoerr
    echoerr
    echoerr "Trapped CTRL-C in ${SELF_FILE}"
  fi
  if [[ "${errorCode}" -eq 0 ]]; then
    echo
    echo
    echo "Everything finished successully!"
    echo
  fi
}

shutdown_on_ctrl_c() {
  export SHUTDOWN="INT"
  exit 141
}

shutdown_on_error() {
  local errorCode="$?"
  # Preserve command causing error
  _BASH_COMMAND="${BASH_COMMAND}"
  cleanup
  if [ -n "${1:-}" ]; then
        errorCode="${1}"
  fi

  echoerr  
  backtrace ${errorCode} >&1
  exit ${errorCode}
}

# param1: mountpoint with full path
is_mounted() {
  mount | awk '{print $3}' | grep -q -e "^$1\$"
}

##
# Wait for confirmation
# param1: question
# param2: default [y|n]
# return: y/n to return code
confirm() {
	local confirmation="${1}"
	local default="${2:-n}"
	local choose
	local answer
	if [ "${default}" == "n" ]; then
		choose="N/y"
	else
		choose="Y/n"
	fi
	echo -en "${C_BRIGHT}${C_BYELLOW}${confirmation} (${choose}) ${C_SAVE}" 
  tput sc
	read answer
	if [ -z "${answer}" ]; then
    tput rc
		echo -e "${C_UP}${default}${C_RETURN}"
	fi
	answer="$(echo ${answer:-${default}} | tr '[:upper:]' '[:lower:]' | cut -c1)"
	echo -ne "${C_RESET}"
	if [ "${answer}" == "y" ]; then
		return 0
	else
		return 1
	fi
}

mkpath() {
  local num_regexp='^[0-9]+$'
  local chmod=""
  local uid="${UID}"
  local gid=""
  local owner=""
  local prefix=""
  local opt_recursive=0
  local chprefix=() 
  local path=""
  local pathes=()
  set -- $*
  while [ $# -gt 0 ]; do
    case "${1}" in
      -o) shift; uid="${1}"; shift;;
      -g) shift; gid="${1}"; shift;;
      -r) shift; chprefix=("$chprefix[@]" -r);;
      -m) shift; chmod="${1}"; shift;;
       *) pathes=("${pathes[*]}" "${1}"); shift;;
    esac
  done
  if [[ -n "${uid}" ]] && [[ ! "${uid}" =~ ${num_regexp} ]]; then uid="$(id -u ${uid})"; fi;
  if [[ -n "${gid}" ]] && [[ ! "${gid}" =~ ${num_regexp} ]]; then gid="$(id -g ${gid})"; fi; 
  if [[ -n "${gid}" ]]; then
    owner="${owner}:${gid}"
  fi
  if [[ "${UID}" != "${uid}" ]]; then
    prefix="sudo"
  fi
  for path in ${pathes[@]}; do
    if [[ ! -d "${path}" ]]; then
      ${prefix} mkdir -p "${path}"
    fi
    if [[ -n "${owner}" ]]; then
      ${prefix} chown "${chprefix}" "${chown}" "${path}"
    fi
    if [[ -n "${chmod}" ]]; then
      ${prefix} chmod "${chprefix}" "${chmod}" "${path}"
    fi
  done
}

mkmount() {
  path="${@:~0}" 
  if ! is_mounted "${path}"; then
    mkpath "${path}"
    # echo "Mounting ${path}"
    MOUNTS=("${MOUNTS[@]}" "${path}")
    execinfo sudo mount ${@}
  else
    echowarn "[Warning] ${path} was already mounted"
  fi
}

echowarn() {
  >&2 echo "${1:-}"
}

echotitle() {
  echo
  echo -e "${C_BRIGHT}${C_UNDERLINE}${1:-}${C_NO_UNDERLINE}${C_RESET}"
  echo
}

echoinfo() {
  echo -e "${C_BRIGHT}${1:-}${C_RESET}"
}

echoerr() {
  echo -e "${C_RED}${1:-}${C_RESET}" >&4
}

raise_error() {
  echoerr "$1"
  exit "${2:-1}"
}

keep_sudo() {
  # Might as well ask for password up-front, right?
  sudo -v

  # Keep-alive sudo: update existing sudo time stamp if set, otherwise do nothing.
  while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
}

umount_sync() {
  busy=1
  echo -n "Unmounting ${1}"
  while [[ "${busy}" -eq 1 ]]; do
    if mountpoint -q "${1}"; then
      sudo umount -l "${1}" && SUCCESS=1 || SUCCESS=0 2> /dev/null
      if [ "${SUCCESS}" -eq 0 ]; then
        busy=0  # umount successful
      else
        # echo -n '.'  # output to show that the script is alive
        sleep .1      # 5 seconds for testing, modify to 300 seconds later on
      fi
    else
      busy=0  # not mounted
    fi
  done
  echo
}

unmount_everything() {
  # Try to unmount everything by /tmp and current working folder
  echo "${MOUNTS[@]}" | tac | tr " " "\n" > "${TMP}/mounts"
  echo >> "${TMP}/mounts"
  MOUNTS=()
  mount | grep -o -E "(^|\s)/[^ ]+" | sed 's/^ //g' | grep -e "^\(/tmp\|${SELF_PATH}\)" |  while IFS= read -r f; do [[ -d "$f" ]] && echo "$f"; done | grep -e "^\(/tmp\|${SELF_PATH}\)" >> "${TMP}/mounts" || true
  # |  while IFS= read -r f; do [[ -d "$f" ]] && echo "$f"; done | grep -e "^\(/tmp\|${SELF_PATH}\)" 
  # 
  # cat "${TMP}/mounts"
  
  cat "${TMP}/mounts" | awk '!x[$0]++' > "${TMP}/mounts.new"
  mv "${TMP}/mounts.new" "${TMP}/mounts"
  if [[ -e "${TMP}/mounts" ]]; then
    while IFS= read -r f; do 
      if [[ -z "${f}" ]]; then
        continue
      fi
      umount_sync "$f" 
    done < "${TMP}/mounts"
  fi
}
##
# Error handlers as early as possible

# trap '' TSTP
trap shutdown_on_error ERR
trap shutdown_on_ctrl_c INT
trap shutdown EXIT

exec 4>&2
exec 3>&1
exec 2> >(pipe_handler_error)

SELF_FILE="${BASH_SOURCE[0]}"
SELF_PATH="$(cd -P "$(dirname "${SELF_FILE}")" && pwd)"
# TMP="$(mktemp -d)"
TMP="${SELF_PATH}/tmp"
mkpath "${SELF_PATH}/tmp"
MOUNTS=()
WORK_PATH="${SELF_PATH}/work"

echoinfo "Working path: ${WORK_PATH}"

# source ${SELF_PATH}/pipe.sh

##
# Const

USER="$(whoami)"
ISO_REMASTER="$(echo "${ISO_MASTER}" | sed -e 's/.iso$/-customized.iso/')"

keep_sudo
unmount_everything

echotitle "Preparing assets"

# Ensure we have GRML tools
if [ ! -e "${SELF_PATH}/bin/grml2usb" ] ; then
  echoinfo "Downloading bin"
  mkpath "${SELF_PATH}/bin"
  curl -s "http://grml.org/grml2usb/grml2usb.tgz" | tar -C "${SELF_PATH}/bin" --strip 1 -xz \
    || raise_error "Could not download grml2usb from http://grml.org/grml2usb/grml2usb.tgz"
else
  echo "grml2iso is present"
fi

if [ ! -f "${SELF_PATH}/iso/${ISO_MASTER}" ] ; then
  echoinfo "Downloading ${ISO_MASTER}"
  wget -q --show-progress -O "${SELF_PATH}/iso/${ISO_MASTER}" "http://download.grml.org/${ISO_MASTER}" \
    || raise_error "Could not download ${ISO_MASTER}"
else
  echo "${ISO_MASTER} is present"
fi


mkpath "${WORK_PATH}/remaster"

# Create config
echotitle "Creating GRML config"
TMP_CONFIG="${TMP}/config"
mkpath "${TMP_CONFIG}/root/.ssh" 
mkpath "${TMP_CONFIG}/home/grml/.ssh" 
sudo rsync -a "${SELF_PATH}/asset/dotfiles/" "${TMP_CONFIG}/root/"
sudo rsync -a "${SELF_PATH}/asset/dotfiles/" "${TMP_CONFIG}/home/grml/"
sudo chmod 0700 "${TMP_CONFIG}/root/.ssh" "${TMP_CONFIG}/home/grml/.ssh"
execinfo cat "/home/${USER}/.ssh/id_rsa.pub" ">>" "${TMP_CONFIG}/root/.ssh/authorized_keys"
execinfo cat "/home/${USER}/.ssh/id_rsa.pub" ">>" "${TMP_CONFIG}/home/grml/.ssh/authorized_keys"
sudo chown -R 1000:1000 "${TMP_CONFIG}/home/grml" 
sudo chown -R 0:0 "${TMP_CONFIG}/root"
# Generating config.tbz
if [[ -e "${WORK_PATH}/remaster/config.tbz" ]]; then sudo rm "${WORK_PATH}/remaster/config.tbz"; fi 
execinfo sudo tar -C "${TMP_CONFIG}" --numeric-owner -jcf "${WORK_PATH}/remaster/config.tbz" .

echotitle "Creating GRML scripts"
mkpath "${WORK_PATH}/remaster/scripts"
sudo rsync -a "${SELF_PATH}/asset/scripts/" "${WORK_PATH}/remaster/scripts"

echotitle "Mounting ISO-Master"
mkpath "${WORK_PATH}/iso"
mkmount -o ro,loop "${SELF_PATH}/iso/${ISO_MASTER}" "${TMP}/iso"
mkpath "${TMP}/squash"
mkpath "${TMP}/overlay"
mkpath "${TMP}/work"
# mkpath -u root "${TMP}/mount"
# sudo ln -s "${SELF_PATH}/provisioners" "${TMP}/mount/provisioners"

# find ISO_MASTER/live/grml64-full
cd "${TMP}/iso" && find . -type f -name "*.squashfs" | sed -e 's/^.\///g' > "$TMP/squashfs"
while read -u5 squashfile; do
  archb="$(echo "${squashfile}" | grep -o -E "[0-9]+" | head -1)"
  arch="x${archb}"
  if [[ "${arch}" = "x32" ]]; then
    arch="x86"
  fi
  squashfile_remaster="${WORK_PATH}/remaster/${squashfile}"

  if [[ -e "${squashfile_remaster}" ]]; then
    if ! confirm "$( basename "${squashfile_remaster}") already exists. Recreate?" "n"; then
      continue
    fi 
    sudo rm -f "${squashfile_remaster}"
  fi

  echotitle "Processing $(basename "${squashfile}") (${arch})"
 
  echoinfo "Generating overlay filesystem"
  IMAGE_PATH="${WORK_PATH}/images"
  mkpath "${IMAGE_PATH}"
  image="${IMAGE_PATH}/overlay.${arch}.img"
  if [ -e "${image}" ]; then
    echo "Using existing $(basename ${image})"
  else
    execinfo dd if=/dev/zero of="${image}" bs=512M count=1
    execinfo mkfs -t ext4 "${image}" 
  fi 
  mkmount -o ro "${TMP}/iso/${squashfile}" "${TMP}/squash"
  mkmount "${image}" "${TMP}/overlay"
  mkpath -o root "${TMP}/overlay/upper" "${TMP}/overlay/work" 
  
  mkmount -t overlay -o "lowerdir=${TMP}/squash,upperdir=${TMP}/overlay/upper,workdir=${TMP}/overlay/work" none "${TMP}/work" 
  
  mkpath -o root "${TMP}/work/asset"
  mkmount -o bind "${SELF_PATH}/asset" "${TMP}/work/asset"

  # sudo a=v chroot /tmp/tmp.OtPM8ztJQl/work /bin/bash -c 'cd /provisioners'

  echoinfo "Prepare chroot"
  mkmount -t proc proc "${TMP}/work/proc"
  mkmount -t sysfs sysfs "${TMP}/work/sys"
  mkmount -o bind /dev "${TMP}/work/dev"
  # mkmount -o bind /dev/null "${TMP}/work/dev/pts"

  # sudo a=v chroot /tmp/tmp.OtPM8ztJQl/work /bin/bash -c 'echo $a'
  
  sudo mv "${TMP}/work/etc/resolv.conf" "${TMP}/work/etc/resolv.conf-grml"
  sudo cp /etc/resolv.conf "${TMP}/work/etc/resolv.conf"
  
  sudo sh -c "echo 'export ARCH=\"${arch}\"' >> \"${TMP}/work/root/.bashrc\""

  # Generic folder I use for mounting backups
  mkpath -o root "${TMP}/work/media/backup"

  echoinfo "Disable GRML testing/live repositories"
  files=("grml-testing.list" "grml-live.list")
  for file in "${files[@]}"; do
    file="${TMP}/work//etc/apt/sources.list.d/${file}"
    if [[ -e "${file}" ]]; then
      echo $file
      sudo sed -i "${file}" -e 's/^\s*\(deb\(-src\)*\)/# \1/g'
    fi
  done
  
  if [[ "${OPT_APT_UPDATE}" -eq 1 ]]; then
    execinfo sudo chroot "${TMP}/work" apt -y update
  fi

  if [[ "${OPT_APT_UPGRADE}" -eq 1 ]]; then
    execinfo sudo chroot "${TMP}/work" apt -y upgrade
  fi
  
  if [[ "${#DEBS}" -gt 0 ]]; then
    execinfo sudo chroot "${TMP}/work" apt -y install ${DEBS[@]}
  fi

  if confirm "Chroot before repackaging squashfs for ${squashfile}?" "n"; then
    echotitle "Chroot into remaster for ${squashfile}"
    sudo ARCH="${arch}" chroot "${TMP}/work"  || true # ignore bad last commando
  fi

  for install_script in $( find "${SELF_PATH}/asset/provisioners" -type f -name "install_*" -exec basename {} \; ); do
    echotitle "Provisioner: ${install_script}"
    sudo ARCH="${arch}" ARCHB="${archb}" chroot "${TMP}/work" /bin/bash -c "cd /asset/provisioners/ && ${install_script}"
  done

  if [[ "${OPT_APT_UPDATE}" -eq 1 ]]; then
    echotitle "Cleaning package cache"
    sudo chroot "${TMP}/work" apt-get -y clean autoclean
    sudo chroot "${TMP}/work" apt-get -y autoremove 
    # sudo chroot "${TMP}/work" rm -rf /var/lib/{apt,dpkg,cache,log}/*
  fi

  # Unmount system directories from chroot
  sudo mv "${TMP}/work/etc/resolv.conf-grml" "${TMP}/work/etc/resolv.conf"  
  umount_sync "${TMP}/work/proc"
  umount_sync "${TMP}/work/sys"
  umount_sync "${TMP}/work/dev"
  umount_sync "${TMP}/work/asset"
  sudo rm -fr "${TMP}/work/asset"
  sudo rm -fr "${TMP}/work/tmp/*"
  
  # Make new squashfs image
  echotitle "Repackaging"
  mkpath "$( dirname "${squashfile_remaster}" )"
  if confirm "Repackage ${squashfile} now?" "y"; then
    execinfo sudo mksquashfs "${TMP}/work" "${squashfile_remaster}"
  fi
  # Unmount work
  umount_sync "${TMP}/work"
  umount_sync "${TMP}/squash" 
  umount_sync "${TMP}/overlay"

done 5< "$TMP/squashfs"

echotitle "Remastering ISO"

# Unmount ISO_MASTER
umount_sync "${TMP}/iso" 

# Check if ISO_MASTER already exists and give confirmation for remaster step
if [[ ! -e "${SELF_PATH}/iso/${ISO_REMASTER}" ]]; then 
  if ! confirm "Everything finished. Remaster ISO now?" "y"; then
    exit 1;
  fi
else
  if confirm "${ISO_REMASTER} already exists. Recreate?" "y"; then
    echo "Removing ${SELF_PATH}/iso/${ISO_REMASTER}"
    sudo rm -f "${SELF_PATH}/iso/${ISO_REMASTER}"
  else
    exit 1
  fi
fi

# Create new ISO_MASTERn
# export GRML2USB="${SELF_PATH}/bin/grml2usb"
cd ${SELF_PATH}/bin
execinfo sudo GRML2USB="./grml2usb" "./grml2iso"  -b "\"$GRMLCODES\"" -c "${WORK_PATH}/remaster/" -o "${SELF_PATH}/iso/${ISO_REMASTER}" "${SELF_PATH}/iso/${ISO_MASTER}"
sudo chown "${USER}" "${SELF_PATH}/iso/${ISO_REMASTER}"

if confirm "Test ISO with qemu (using kvm)?" "y"; then
  qemu-system-x86_64 -boot d -cdrom "${SELF_PATH}/iso/${ISO_REMASTER}" -m 2048 -enable-kvm
fi

if "Cleanup working directory?" "y"; then
    if [[ -d "$(WORK)" ]]; then execinfo sudo rm -fr "$(WORK_PATH)"; fi;
fi


cleanup

echoinfo
echoinfo "Your new ISO is ready at iso/${ISO_REMASTER}"
# echoinfo
# echoinfo "Use bin/grml2usb to install to your USB device\!"

exit 0
