# GRML ISO Remaster

## A Custom Flavor for Offical ISOs

Easily remaster GRML ISO by bash script and `grml2iso` and `grml2usb` tools. The script is loosely based on https://gist.github.com/psi-4ward/4121143240ef9e4746a6.

### Features

* Use overlayfs to mount iso, then squashfs files

* Use overlay to make installations by 

  * either by *provisioners* (asset/provisioners)
  * or install packages by *chroot*
  * repackage squashfs then ISO

* Include your `.ssh/*.pub` files into `authorized_keys`
* Inject [Grml Cheats](http://git.grml.org/?p=grml-live.git;a=blob_plain;f=templates/GRML/grml-cheatcodes.txt;hb=HEAD)
* Auto download all necessary tools (ISO and grml2usb)

### Defaults

* ISO:  `grml96-full_2018.12.iso`
* Packages:  `clonezilla`, `ripgrep`, `byobu`, `task-spooler`
* Other Binaries: `veracrypt`, `micro` 
* Cheats:
  * `ssh=grml` Start SSH Server and set the root password to `grml`
  * `lang=de keyboard=de` German
  * `nobeep` Suppress beep sound on boot
  * `noblank` Disable screen energy saving
  * `noquick` Disable GRML config tool
  * `startup=/usr/bin/ip-screen` Show IP-Addresses after boot

### Remarks

I do not use `grml-live` or GRML `debs` boot params since I want the packages present right after boot and will not always will have network on the machines.

There are not really plans to improve the script since it works for my needs so far. 

## Ideas / TODOs

- [ ] Automount writeable USB partition 
- [ ] Fix: keep sudo activated (sometimes does timeout)

## Built-in Support

  * Custom GRML configs (`config`boot param, config.tbz)
  * Custom GRML scripts (`scripts`boot param)
  * Every other GRML boot parameters

## Pathes

| Path               | Descriptin                            |
| ------------------ | ------------------------------------- |
| asset/dotfiles     | These files will be placed into shell |
| asset/provisioners | Scripts to be run in chroot           |
| asset/scripts      | Scripts for GRML                      |

The Asset-Directory will be mounted in a temporary folder

## Instructions

 Please review `grml-iso-remaster` since I did not have time to document properly.

### Suggestions

- Install `xorriso` ISO creation tool in order to support UEFI (already supported by grml2iso)

### Write ISO to USB

```bash
# without progress
sudo dd if=iso/grml.iso of=/dev/sdX bs=64
# with progress
sudo dd if=iso/grml.iso | pv | sudo of=/dev/sdX bs=64
# using GRML tool
sudo bin/grml2usb iso/grml96-full_2018.12-customized.iso /dev/sdX#
```

#### Test ISO using QEMU

```bash
qemu-system-x86_64 -boot d -cdrom iso/image.iso -m 512 -enable-kvm
```


KVM is suggested since emulation may be very slow.

## Trackback

These links may be a useful read

* [GRML Config Tool](http://grml.org/online-docs/grml-config.html)
* [GRML USB](https://wiki.grml.org/doku.php?id=usb)
* [GRML Boot Cheatcodes](http://git.grml.org/?p=grml-live.git;a=blob_plain;f=templates/GRML/grml-cheatcodes.txt;hb=HEAD)
* [GRML hardware issues and how to resolve](https://wiki.grml.org/doku.php?id=hardware)
* [YouTube:  How to use GRML and zsh (de)](https://www.youtube.com/watch?v=KdWWbR3PtrY)